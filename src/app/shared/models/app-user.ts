export interface AppUser {
  name: string;
  lastname: string;
  email: string;
}
