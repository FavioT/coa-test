import { Product } from './product';

export class ShoppingCartItem {
  $key: string;
  id: number;
  name: string;
  description: string;
  price: number;
  quantity: number;
  imageUrl: string;
  key: string;

  constructor(init?: Partial<ShoppingCartItem>) {
    Object.assign(this, init);
  }

  get totalPrice(): number {
    return this.price * this.quantity;
  }
}
