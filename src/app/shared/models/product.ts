export interface Product {
  name: string;
  price: number;
  description: string;
  imageUrl: string;
  $key: string;
  key: string;
}
