import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { ActivatedRoute } from '@angular/router';
import { AppUser } from 'shared/models/app-user';
import { switchMap, map } from 'rxjs/operators';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private db: AngularFireDatabase,
    private route: ActivatedRoute) {
  }

  login(loginForm: any): Observable<boolean> {
    return this.db.list('/users').valueChanges().pipe(
      map((users: any) => {
        const findUser = users.find((user: any) => {
          return user.email === loginForm.email;
        });
        if (findUser && (findUser.password === loginForm.password)) {
          this.store(findUser);
          return true;
        } else {
          return false;
        }
      })
    ) as Observable<boolean>;
  }

  logout(): void {
    localStorage.removeItem('user');
  }

  store(user: AppUser): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  getUser(): Observable<AppUser | null> {
    return of(JSON.parse(localStorage.getItem('user')) as AppUser);
  }
}
