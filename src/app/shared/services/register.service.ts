import { RegisterForm } from './../../core/components/register/register.component';
import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AbstractControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {AuthService} from 'shared/services/auth.service';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(
    private db: AngularFireDatabase,
    private auth: AuthService
  ) {
    this.auth.getUser().subscribe(user => console.log(user));
  }

  registerUser(form: any): Observable<boolean> {
    this.db.list('users').push({
      name: form.name,
      lastname: form.lastname,
      email: form.email,
      password: form.password
    });
    return of(true);
  }

}
