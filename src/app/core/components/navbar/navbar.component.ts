import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { AppUser } from 'shared/models/app-user';
import { AuthService } from 'shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ShoppingCart } from 'shared/models/shopping-cart';
import { Observable } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  cart$: Observable<ShoppingCart>;
  user$ = this.auth.getUser();

  constructor(
    private auth: AuthService,
    private shoppingCartService: ShoppingCartService,
    private snackBar: MatSnackBar)
  {

  }

  async ngOnInit(): Promise<void> {
    this.cart$ = await this.shoppingCartService.getCart();
  }

  logout(): void {
    this.auth.logout();
    this.snackBar.open('Ha cerrado sesión.', 'Cerrar');
  }
}
