import { AuthService } from 'shared/services/auth.service';
import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

export type LoginForm = {
  email: string;
  password: string;
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm = new FormGroup({});
  model = {};
  loginFormFields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Dirección de correo',
        placeholder: 'Ingrese su email',
        required: true,
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        label: 'Contraseña',
        placeholder: 'Ingrese su contraseña.',
        required: true,
      }
    }
  ];

  constructor(
    private auth: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
    ) {
  }

  onLogin(): void {
    this.auth.login(this.model).subscribe(result => {
      if (result) {
        this.snackBar.open('Se ha logueado correctamente.', 'Cerrar');
        this.router.navigate(['/']);
      } else {
        this.snackBar.open('Verifique el email y la contraseña ingresada.', 'Cerrar');
      }
    });
  }

}
