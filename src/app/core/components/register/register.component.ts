import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RegisterService} from 'shared/services/register.service';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormlyFieldConfig} from '@ngx-formly/core';

export type RegisterForm = {
  name: string;
  lastname: string;
  email: string;
  password: string;
};

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({});
  model = {};
  registerFormFields: FormlyFieldConfig[] = [
    {
      key: 'name',
      type: 'input',
      templateOptions: {
        label: 'Nombre',
        placeholder: 'Ingrese su nombre.',
        required: true,
      }
    },
    {
      key: 'lastname',
      type: 'input',
      templateOptions: {
        label: 'Apellido',
        placeholder: 'Ingrese su apellido.',
        required: true,
      }
    },
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Dirección de correo',
        placeholder: 'Ingrese un email',
        required: true,
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        label: 'Contraseña',
        placeholder: 'Ingrese una contraseña.',
        required: true,
      }
    }
  ];

  constructor(
    private registerService: RegisterService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  }

  onRegister(): void {
    this.registerService.registerUser(this.model).subscribe( _ => {
      this.snackBar.open('¡Gracias por registrarse!', 'Cerrar');
      this.router.navigate(['/']);
    });
  }

}
