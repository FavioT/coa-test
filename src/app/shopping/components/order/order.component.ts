import { AuthService } from 'shared/services/auth.service';
import { Subscription } from 'rxjs';
import { ShoppingCart } from 'shared/models/shopping-cart';
import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CurrencyPipe} from '@angular/common';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
  providers: [
    CurrencyPipe
  ]
})
export class OrderComponent implements OnInit, OnDestroy {
  shipping: any;
  cart: ShoppingCart;
  cartSubscription: Subscription;
  userId: string;
  userSubscription: Subscription;

  columnDefs = [
    { field: 'items' },
    { field: 'precio' }
  ];

  rowData: any[];
  domLayout = 'autoHeight';

  constructor(
    private router: Router,
    private shoppingCartService: ShoppingCartService,
    private authService: AuthService,
    private cp: CurrencyPipe
  ) {}

  async ngOnInit(): Promise<void> {
    const cart$ = await this.shoppingCartService.getCart();
    this.cartSubscription = cart$.subscribe(cart => {
      this.cart = cart;
      this.rowData = this.cart.items.map(item => {
        return {
          items: item.quantity + ' x ' + item.name,
          precio: this.cp.transform(item.totalPrice, 'USD', true)
        };
      });
      this.rowData.push({
        items: 'Total',
        precio: this.cp.transform(this.cart.totalPrice, 'USD', true)
      });
    });
    // this.userSubscription = this.authService.user$.subscribe(user => this.userId = user.uid);
  }

  ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
    // this.userSubscription.unsubscribe();
  }
}
