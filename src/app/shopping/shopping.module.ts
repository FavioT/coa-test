import { SharedModule } from 'shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { OrderComponent } from './components/order/order.component';
import { ProductsComponent } from './components/products/products.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    ProductsComponent,
    ShoppingCartComponent,
    OrderComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {path: 'products', component: ProductsComponent},
      {path: 'shopping-cart', component: ShoppingCartComponent},
      {path: 'order', component: OrderComponent},
    ]),
    MatButtonModule,
    MatCardModule,
    MatListModule,
    AgGridModule.withComponents([])
  ]
})
export class ShoppingModule { }
